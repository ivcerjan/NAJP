<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>  
<!DOCTYPE html>
<html>
 <head>
    <meta charset="UTF-8">
    <title>Lista proizvoda</title>
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
      rel="stylesheet">
 </head>
 <body>

    <jsp:include page="_header.jsp"></jsp:include>
    <jsp:include page="_menu.jsp"></jsp:include>

    <h3>Lista proizvoda</h3>

    <p style="color: red;">${errorString}</p>
    
    <table class="table">
       <thead>
	       <tr>
		       <th>Sifra</th>
		       <th>Naziv</th>
		       <th>Cijena</th>
		       <th>Uredi</th>
		       <th>Izbrisi</th>
	       </tr>
       </thead>
     
	<tbody>
     	<c:forEach items="${productList}" var="product" >
          <tr>
             <td>${product.code}</td>
             <td>${product.name}</td>
             <td>${product.price}</td>
             <td>
                <a href="editProduct?code=${product.code}">Edit</a>
             </td>
             <td>
                <a href="deleteProduct?code=${product.code}">Delete</a>
             </td>
          </tr>
       </c:forEach>
   	</tbody>
       </table>

    <a href="createProduct" >Dodaj novi proizvod</a>

 </body>
</html>