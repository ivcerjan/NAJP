<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
 <head>
    <meta charset="UTF-8">
    <title>Uredi proizvod</title>
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
      rel="stylesheet">
 </head>
 <body>

    <jsp:include page="_header.jsp"></jsp:include>
    <jsp:include page="_menu.jsp"></jsp:include>

    <h3>Uredi proizvod</h3>

    <p style="color: red;">${errorString}</p>

    <c:if test="${not empty product}">
       <form method="POST" action="doEditProduct">
          <input type="hidden" name="code" value="${product.code}" />
          <table class="table">
             <tr>
                <td>Sifra</td>
                <td style="color:red;">${product.code}</td>
             </tr>
             <tr>
                <td>Naziv</td>
                <td><input type="text" name="name" value="${product.name}" /></td>
             </tr>
             <tr>
                <td>Cijena</td>
                <td><input type="text" name="price" value="${product.price}" /></td>
             </tr>
             <tr>
                <td colspan = "2">
                    <input type="submit" value="Submit" />
                    <a href="${pageContext.request.contextPath}/productList">Cancel</a>
                </td>
             </tr>
          </table>
       </form>
    </c:if>

 </body>
</html>