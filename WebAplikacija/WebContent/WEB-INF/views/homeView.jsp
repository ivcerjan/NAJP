<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>

<html>
  <head>
     <meta charset="UTF-8">
     <title>Pocetna</title>
     <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
      rel="stylesheet">
  </head>
  <body>

     <jsp:include page="_header.jsp"></jsp:include>
     <jsp:include page="_menu.jsp"></jsp:include>
   
      <h3>Pocetna stranica</h3>
     
      Ovo je primjer web aplikacije koji koristi jsp, servlete i Jdbc(oracle). <br><br>
      Na stranicu se moze ulogirati, spremati informacije korisnika u cookie 
      te izvoditi CRUD operacije nad tablicom Products.

  </body>
</html>