<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<!DOCTYPE html>
<html>
 <head>
    <meta charset="UTF-8">
    <title>Novi proizvod</title>
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
      rel="stylesheet">
 </head>
 <body>

    <jsp:include page="_header.jsp"></jsp:include>
    <jsp:include page="_menu.jsp"></jsp:include>
   
    <h3>Novi proizvod</h3>
   
    <p style="color: red;">${errorString}</p>
   
    <form method="POST" action="doCreateProduct">
       <table class="table">
          <tr>
             <td>Sifra</td>
             <td><input type="text" name="code" value="${product.code}" /></td>
          </tr>
          <tr>
             <td>Naziv</td>
             <td><input type="text" name="name" value="${product.name}" /></td>
          </tr>
          <tr>
             <td>Cijena</td>
             <td><input type="text" name="price" value="${product.price}" /></td>
          </tr>
          <tr>
             <td colspan="2">                  
                 <input type="submit" value="Unesi" />
                 <a href="productList">Odustani</a>
             </td>
          </tr>
       </table>
    </form>
   
 </body>
</html>