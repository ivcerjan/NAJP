<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
 <head>
    <meta charset="UTF-8">
    <title>Podaci o korisniku</title>
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
      rel="stylesheet">
 </head>
 <body>

    <jsp:include page="_header.jsp"></jsp:include>
    <jsp:include page="_menu.jsp"></jsp:include>

    <h3>Ulogiran: ${user.userName}</h3>

    Korisnicko ime: <b>${user.userName}</b>
    <br />
    Sifra: ${user.password } <br />
    <br />
    Spol: ${user.gender } <br />

 </body>
</html>